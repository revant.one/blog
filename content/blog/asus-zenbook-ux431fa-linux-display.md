---
title: "Asus Zenbook UX431FA Linux display"
date: 2019-07-21
draft: false
---

I followed this [comment](https://bugs.launchpad.net/ubuntu/+source/xorg-server/+bug/1821533/comments/16)

First boot with `nomodeset` to get access to system.

As root change working directory to `/lib/firmware/edid/`

```sh
cd /lib/firmware/edid
```

Download the edid file

```sh
wget -c https://github.com/akatrevorjay/edid-generator/raw/master/1920x1080.bin -O ncp.bin
```

As root modify the kernel boot parameter and update grub menu

```sh
nano /etc/default/grub

###
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash drm.edid_firmware=edid/ncp.bin"
###

update-grub2 # for ubuntu
```

Reboot and the display and brightness control will work
