---
title: "Just a bump for amby"
date: 2014-05-26
draft: false
---

<p style="text-align: center;"><img src="/MumbaiMirror20140526-pg10.jpg"><br></p>
<p><em>Dipu Kaka</em> (My uncle Pradeep Nandgaonkar) loves his cars. Initially he only owned&nbsp;Minor Morris 1000. My Grandfather passed down the legacy of Minor 1000 to my uncle.</p>
<p>Speaking of my grandfather, he loved his Minor 1000 and I traveled in Morris Minor 1000 along with my grandfather many times.</p>
<p>Hindustan Ambassador has a&nbsp;lineage of&nbsp;Morris Oxford III (1956-59).&nbsp;Owning 1957 Amby MK1 is just like owning Morris Oxford III original. You can compare Amby with&nbsp;contemporary Merc and Morris. Further,&nbsp;Amby&nbsp;also has its own&nbsp;<em>made in India</em> charm.&nbsp;My uncle restored the Amby&nbsp;out of his passion for Vintage cars, especially Morris.</p>
<p>You can see him in VCCCI rallies in Mumbai almost every year. Both of his vintage beauties have won 1st prizes as well as other prizes.</p>
<p>One of the first few Amby can be restored to stock without any modifications and It works perfect! You know&nbsp;It is just a bump for amby.</p>
<p>No. 1 Amby in Mumbai and its owner –&nbsp;<em>Dipu Kaka</em> gets all the due attention in today’s Mumbai Mirror <a href="http://www.mumbaimirror.com/mumbai/others/Just-a-bump-for-Amby/articleshow/35615157.cms" target="_blank">article</a>&nbsp;on page 10.</p>
<p><a href="http://epaperbeta.timesofindia.com/Article.aspx?eid=31821&amp;articlexml=JUST-A-BUMP-FOR-AMBY-26052014010007" target="_blank">http://epaperbeta.timesofindia.com/Article.aspx?eid=31821&amp;articlexml=JUST-A-BUMP-FOR-AMBY-26052014010007</a></p>