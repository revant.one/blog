Revant is an Indian name for a boy. You can find more about the name on <a href="https://en.wikipedia.org/wiki/Revanta" target="_blank">wikipedia</a>.

A Computer Geek - people call me with tech queries

Web apps, microservices and serverless

Ecologically sustainable living enthusiast

MBA in Marketing.

Takes care of Health and Fitness; I follow a conditioning routine and eat only natural and minimum processed food.

[more ...](/blog)
