### Create namespace and namespace user

reference : https://jeremievallee.com/2018/05/28/kubernetes-rbac-namespace-user.html

```sh
kubectl create -f yaml/revant-access.yaml
```

### Get secrets

```sh
kubectl describe sa revant-user -n revant

revant-user-token-xxxxx
```

### Get tokens

```sh
kubectl get secret revant-user-token-xxxxx -n revant -o "jsonpath={.data.token}" | base64 -d
```

### Get Certificate

```sh
kubectl get secret revant-user-token-xxxxx -n revant -o "jsonpath={.data['ca\.crt']}" | base64 -d
```

### Create Kube config

```yaml
# revant-config.yaml
apiVersion: v1
kind: Config
preferences: {}

# Define the cluster
clusters:
- cluster:
    certificate-authority-data: PLACE CERTIFICATE HERE
    # You'll need the API endpoint of your Cluster here:
    server: https://YOUR_KUBERNETES_API_ENDPOINT
  name: my-cluster

# Define the user
users:
- name: revant-user
  user:
    as-user-extra: {}
    client-key-data: PLACE CERTIFICATE HERE
    token: PLACE USER TOKEN HERE

# Define the context: linking a user to a cluster
contexts:
- context:
    cluster: my-cluster
    namespace: revant
    user: revant-user
  name: revant

# Define current context
current-context: revant
```

### Pack Config for gitlab

```sh
cat revant-config.yaml | base64 | pbcopy
```
