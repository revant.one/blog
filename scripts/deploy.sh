#!/bin/bash

# Check helm chart is installed or create
# reuse installed values and resets data

export CHECK_AS=$(helm ls --namespace revant -q | grep revant-blog)
if [ "$CHECK_AS" = "revant-blog" ]
then
    echo "Updating existing revant-blog . . ."
    helm upgrade revant-blog \
        --namespace revant \
        --reuse-values \
        helm-chart/revant-blog
fi
