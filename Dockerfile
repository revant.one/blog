FROM bitnami/minideb:latest as builder

COPY . /var/www/blog

# Install packages
RUN install_packages \
    python-pygments \
    git \
    ca-certificates \
    asciidoc \
    wget

# Download and install hugo
ENV HUGO_VERSION 0.55.6
ENV HUGO_BINARY hugo_${HUGO_VERSION}_Linux-64bit.deb

RUN wget -O /tmp/hugo.deb \
    https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BINARY} \
    && dpkg -i /tmp/hugo.deb \
    && rm /tmp/hugo.deb \
    && mkdir -p /var/www/blog

WORKDIR /var/www/blog
RUN hugo

# Distributable image layer
FROM nginx:alpine
COPY --from=builder /var/www/blog/public/ /usr/share/nginx/html
COPY ./docker/nginx.conf /etc/nginx/conf.d/default.conf
