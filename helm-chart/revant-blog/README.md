### Install

```sh
helm install --name revant-blog \
    --tiller-namespace revant \
    --namespace revant \
    revant-blog
```

### Uninstall

```sh
helm del --purge revant-blog
```
